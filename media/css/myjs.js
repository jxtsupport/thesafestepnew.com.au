/*
=================== 
1. Structuring with bootstrap classes
2. Global Changes
3. Job Board Design
4. Plugin calls
5. Supporting function
6. Click Methods & related function calls
7. Rss Feed
8. System pages changes
 8.1. News page changes
*/

!(function($) {
    // RSS feed publish date format
    function formatDate(pubDate) {
        var monthList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        //var dateObj = new Date(Date.parse(pubDate));
        var dateObj = pubDate.split('/'),
            mnth = monthList[parseInt(dateObj[1]) - 1],
            myDay = "<span class='rss-item-pubDate-date'>" + dateObj[0] + "</span> ",
            myMonth = "<span class='rss-item-pubDate-month'>" + mnth + "</span> ",
            myYear = "<span class='rss-item-pubDate-full-year'>" + dateObj[2] + "</span> ";
        return myDay + '<br>' + myMonth;
    }

    // jquery
    $(function() {

        // 1. Structuring with bootstrap classes
        // ========================================
        // Section > Div.container
        $('#dynamic-container, #content-container, #job-dynamic-container').addClass('container');

        // Content column
        if ($.trim($('#dynamic-side-left-container, #side-left').html()).length) {
            $('#dynamic-content, #content-container #content').addClass('col-sm-8 col-md-9');
            $('#dynamic-side-left-container, #side-left').addClass('col-sm-4 col-md-3');
        } else {
            $('#dynamic-content, #content-container #content').addClass('col-xs-12');
            $('#dynamic-side-left-container, #side-left').addClass("hidden");
        }
        $('#job-dynamic-container #content').addClass('col-xs-12');

        // form elements style
        $('input:not([type=checkbox]):not([type=radio]):not([type=submit]):not([type=reset]):not([type=file]):not([type=image]):not([type=date]), select, textarea').addClass('form-control');
        $('input[type=text]').addClass('form-control');
        $('input[type=submit]').addClass('btn btn-primary');
        $('.mini-new-buttons').addClass('btn btn-primary');
        $('input[type=reset]').addClass('btn btn-default');

        // Responsive table
        $('.dynamic-content-holder table, .content-holder table').addClass('table table-bordered').wrap('<div class="table-responsive"></div>');

        // Convert top menu to Boostrap Responsive menu
        $('.navbar .navbar-collapse > ul').addClass('nav navbar-nav');
        $('.navbar .navbar-collapse > ul > li').has('ul').addClass('dropdown');
        $('.navbar .navbar-collapse > ul > li.dropdown > a').addClass('disabled');
        $('.navbar .navbar-collapse > ul > li.dropdown').append('<a id="child-menu"></a>');
        $('.navbar .navbar-collapse > ul > li.dropdown > a#child-menu').append('<b class="caret"></b>').attr('data-toggle', 'dropdown').addClass('dropdown-toggle');
        $('.navbar .navbar-collapse > ul > li > ul').addClass('dropdown-menu');

        // add placeholder for search widget text field
        $('#keywords1').attr('placeholder', 'Keywords');
        $('#locationID1 option[value="-1"]').text('Location');

        // 2. Global Changes
        // ========================================

        // Adding a aria-label for accessabilty (Temp fix ONLY)
        $('#keywords1').attr('aria-label', 'Keyword search box');
        $('#professionID1').attr('aria-label', 'Professions');
        $('#locationID1').attr('aria-label', 'Locations');


        $('link[href="/media/COMMON/newdash/lib/bootstrap.min.css"]').remove();

        var currentPage = window.location.pathname.toLowerCase();

        // remove empty li's on the system pages. 
        $("#side-left li:empty").remove();

        // remove empty left side bar
        if ($('#prefix_left-navigation').children().length == 0) {
            $('#prefix_left-navigation').remove();
        }
        if ($('#side-left').children().length == 0) {
            $('#side-left').remove();
        }

        // add active class to links.
        $("li a[href='" + window.location.pathname.toLowerCase() + "']").parent().addClass("active");
        $("li.active li.active").parent().closest("li.active").removeClass("active");
        $("li li.active").parent().parent().addClass("active")

        // add last-child class to navigation 
        $("#prefix_navigation > ul > li:last").addClass("last-child");

        // add btn style
        $(".backtoresults a").addClass("btn btn-default");
        $(".apply-now-link a").addClass("btn btn-primary");
        $(".button a").addClass("btn btn-default");

        //.left-hidden show
        if ((document.URL.indexOf("/advancedsearch.aspx") >= 0)) {
            $(".left-hidden").css("display", "block");
        }
        if ((document.URL.indexOf("/advancedsearch.aspx?") >= 0)) {
            $(".left-hidden").css("display", "none");
        }
        if ((document.URL.indexOf("/member/createjobalert.aspx") >= 0)) {
            $(".left-hidden").css("display", "block");
        }
        if ((document.URL.indexOf("/member/login.aspx") >= 0)) {
            $(".left-hidden").css("display", "block");
        }

        // Contact - Google map
        $("#footer").prepend($("#contact-map"));


        // generate select navigation from sidebar Dynamic menu
        $("#dynamic-content").convertNavigation({
            title: "Related Pages",
            links: "#site-topnav .navbar-nav li.active a:not([data-toggle=dropdown])"
        });

        // generate actions button on Job Listing page
        $(".job-navbtns").convertButtons({
            buttonTitle: "Actions&hellip;",
            title: "Please choose&hellip;",
            links: ".job-navbtns a"
        });

        // generate filters button on Job Listing page
        $(".job-navbtns").convertFilters({
            buttonTitle: "Filters&hellip;",
            filteredTitle: "Applied Filters",
            title: "Please choose&hellip;",
            filtered: ".search-query p",
            list: "ul#side-drop-menu",
            excludeFromList: "#AdvancedSearchFilter_PnlCompany"
        });

        /* System Page Forms */
        if (currentPage == "/member/createjobalert.aspx") {
            setTimeout('__doPostBack(\'ctl00$ContentPlaceHolder1$ucJobAlert1$ddlProfession\',\'\')', 0);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() {
                $('.alternate > li > select, #ctl00_ContentPlaceHolder1_ucJobAlert1_txtSalaryLowerBand, #ctl00_ContentPlaceHolder1_ucJobAlert1_txtSalaryUpperBand').addClass('form-control');
                $('#ctl00_ContentPlaceHolder1_ucJobAlert1_ddlProfession, #ctl00_ContentPlaceHolder1_ucJobAlert1_ddlRole, #ctl00_ContentPlaceHolder1_ucJobAlert1_ddlLocation, #ctl00_ContentPlaceHolder1_ucJobAlert1_lstBoxArea, #ctl00_ContentPlaceHolder1_ucJobAlert1_ddlSalary').addClass('form-control');
            });
        }
        $(document).ajaxComplete(function() {
            $('#divRoleID1 > select, #divAreaDropDown1 > div > select').addClass('form-control');
            $('#divRoleID > select, #divAreaDropDown > div > select').addClass('form-control');
        });
        $('#salaryID').change(function() {
            $(document).ajaxComplete(function() {
                $('#divSalaryFrom > input').addClass('form-control');
                $('#divSalaryTo > input').addClass('form-control');
            });
        });

        function SalaryFromChange1() {
            $(document).ajaxComplete(function() {
                $('#divSalaryTo1 > input').addClass('form-control');
                $('#divSalaryFrom1 > input').addClass('form-control');
            });
        }

        if (currentPage == "/member/register.aspx") {
            $(".uniForm").addClass("border-container");
        }
        if (currentPage == "/member/createjobalert.aspx") {
            $(".uniForm").addClass("border-container");
        }

    });

    // Resize action
    /*$(window).on('resize', function() {

    	var wi = $(this).width();

    	// Mobile & Tablet
    	if ( wi <= 992 ) {
    		//$('#dynamic-side-left-container').before($('#dynamic-content'));
    		//$('#side-left').before($('#content'));    		
    		$('.navbar .navbar-collapse > ul > li.dropdown > a').removeAttr('class');
    	}
    	//  Desktop
    	else {
    		//$('#dynamic-side-left-container').after($('#dynamic-content'));
    		//$('#side-left').after($('#content'));
    		$('.navbar .navbar-collapse > ul > li.dropdown > a').addClass('disabled');
    	} 

    });*/

    $(document).ready(function() {
        /*// Resize action
        var $window = $(window);
        	// Function to handle changes to style classes based on window width
        	function checkWidth() {
        	if ($window.width() < 992) {
        		$('.navbar .navbar-collapse > ul > li.dropdown > a').removeAttr('class');	
        		}
        }
        	// Execute on load
        	checkWidth();			
        	// Bind event listener
        	$(window).resize(checkWidth);*/

        // 4. Plugin calls
        // ========================================

        // Home services - carousel
        // $('.t-gallery').Gallerycarousel({ autoRotate: 4000, visible: 4, speed: 1200, easing: 'easeOutExpo', itemMinWidth: 250, itemMargin: 30 })

        //add class to body in home page
        if ($(".main-banner").length) { $("body").addClass("home-page"); }

        //add class to body as per url
        var pageTitle = window.location.pathname.replace("/", "");
        if (pageTitle != "") {
            if (pageTitle.indexOf('/') > -1) {
                pageTitle = pageTitle.replace(/\//g, "-");
            }
            $("body").addClass(pageTitle);
        }

        //change banner img
        var link = $('.inner-banner-img').attr("style");
        $('.inner-banner').attr("style", link);


        //change contact page banners
        $(".contact .nav-tabs li a").click(function() {
            var bannerlink = $(this).parent().find('.contact-img').attr('style');
            $('.inner-banner').attr("style", bannerlink);
        });

        //add class in left navigation links
        $("#prefix_left-navigation ul li a").each(function() {
            var add_class = $(this).attr("href").split("/"),
                class_name = add_class[add_class.length - 1].toLowerCase();
            $(this).addClass(class_name);
        });

        // Equal Height	
        $.fn.eqHeights = function(options) {

            var defaults = { child: false };
            var options = $.extend(defaults, options);
            var el = $(this);
            if (el.length > 0 && !el.data('eqHeights')) {
                $(window).bind('resize.eqHeights', function() {
                    el.eqHeights();
                });
                el.data('eqHeights', true);
            }
            if (options.child && options.child.length > 0) {
                var elmtns = $(options.child, this);
            } else {
                var elmtns = $(this).children();
            }

            var prevTop = 0;
            var max_height = 0;
            var elements = [];
            elmtns.height('auto').each(function() {

                var thisTop = this.offsetTop;
                if (prevTop > 0 && prevTop != thisTop) {
                    $(elements).height(max_height);
                    max_height = $(this).height();
                    elements = [];
                }
                max_height = Math.max(max_height, $(this).height());
                prevTop = this.offsetTop;
                elements.push(this);
            });

            $(elements).height(max_height);
        };

        // Equal Height - Usage
        $('.service-holder').eqHeights();

        // 5. Supporting function
        // ========================================
        // if there is a hash, scroll down to it. Sticky header covers up top of content.
        if ($(window.location.hash).length) {
            $("html, body").animate({
                scrollTop: $(window.location.hash).offset().top - $(".navbar-wrapper").height() - 40
            }, 100);
        }

        // 6. Click Methods & related function calls
        // ========================================
        // contact page stop scrolling until clicked.
        $(".r27_map-overlay").click(function() {
            $(this).hide();
        });

        // 7. Rss Feed
        // ========================================
        $(".teamList-inner").each(function() {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/ConsultantsRSS.aspx"],
                    limit: 200,
                    addNBSP: false,
                    repeatTag: "consultant"
                },
                templates: {
                    itemTemplate: '<div class="col-md-3 col-sm-4"><div class="consultant-box"><div class="team-social"><div class="social-list"><a href="mailto:{{Email}}" title="Email"><em class="fa fa-envelope"><span class="hidden">Email</span></em></a><a href="tel:{{Phone}}" title="Call"><em class="fa fa-phone"><span class="hidden">Phone</span></em></a><a href="{{LinkedInURL}}" title="Linkedin"><em class="fa fa-linkedin"><span class="hidden">Linkedin</span></em></a></div><figure><img src="{{ImageURL}}" alt="{{FirstName}} {{LastName}}"/></figure></div><h2><a href="/t/{{FriendlyURL}}" title="{{FirstName}} {{LastName}}"><span class="name">{{FirstName}} {{LastName}}</span></a></h2><span class="position">{{PositionTitle}}</span></div></div>'
                },
                complete: function() {
                    // Callback
                    /* Arrange all the consultants alphabetically as per the client's request */ 
                    $('.team-tab1').data("sortKey", "span.name"); $(".team-tab1").click(function() {
                        sortUsingNestedText($('.teamList-inner'), "div.col-md-3", $(this).data("sortKey"));
                    });
                    /* Arrange all the consultants alphabetically as per the client's request */    
                }
            });
        });



        //inner team testimonial slider
        $(".inner-team-testimonial").slick({
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 5000
        });


        // Latest Jobs widget
        $("#myJobsList ul").includeFeed({
            baseSettings: { rssURL: "/job/rss.aspx?search=1&addlocation=1&addworktype=1", addNBSP: false, },
            elements: { title: 1, description: 1 },
            predicates: {
                pubDate: formatDate
            },
            complete: function() {
                if ($(this).children().length > 1) {
                    $(this).slick({
                        dots: false,
                        infinite: true,
                        speed: 2500,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        arrows: false,
                        autoplay: true,
                        autoplaySpeed: 500,
                        responsive: [{
                                breakpoint: 1199,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 991,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 767,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });

                }

            }
        });


        //hide testimonial in inner team page if testimonial is not there
        len = $(".testimonial-text").text().replace(/\s/g, '').length;
        if (len <= 0) { $(".team-testimonials").hide(); }

        // Latest Jobs widget
        // $("#joblist").each(function() {
        //     var dataURL = $(this).attr("data-url");
        //     $(this).includeFeed({
        //         baseSettings: {
        //             rssURL: [dataURL || "/job/rss.aspx?addlocation=1&addworktype=1"],
        //             addNBSP: false
        //         },
        //         templates: {
        //             itemTemplate: "<div class='job-box'><h3><a href='{{link}}'>{{title}}</a></h3><p class='hidden'>{{description}}</p></div>"
        //         },
        //         predicates: {
        //             pubDate: formatDate
        //         },
        //         complete: function() {

        //         }
        //     });
        // });
        //customer feedback
        $("#testimonial").each(function() {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/newsrss.aspx?category=testimonials"],
                    limit: 200,
                },
                templates: {
                    itemTemplate: '<div class="testimonial-block"><p>{{description}}</p></div>'
                },
                complete: function() {
                    if ($(this).children().length > 1) {
                        $(this).slick({
                            dots: true,
                            infinite: true,
                            speed: 500,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                            autoplay: true,
                            autoplaySpeed: 5000
                        });
                    }
                }
            });
        });

        //testimonials
        $("#testimonial-inner").each(function() {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/newsrss.aspx?category=testimonials"],
                    limit: 200,
                },
                templates: {
                    itemTemplate: '<div class="testimonial-content"><p>{{description}}</p></div>'
                },
                complete: function() {
                    if ($(this).children().length > 1) {
                        $(this).slick({
                            dots: true,
                            infinite: true,
                            speed: 500,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                            autoplay: true,
                            autoplaySpeed: 5000
                        });
                    }
                }
            });
        });
        // New Testimonial slider added

        $(".testimonial-text").each(function() {
            if ($('.testimonial-text p').length) {
                $('.testimonial-text').slick({
                    dots: true,
                    infinite: true,
                    speed: 500,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    autoplay: true,
                    autoplaySpeed: 5000
                })
            } else {
                $('.testimonial-cont').append('<p>There are no testimonials</p>')
            }
        });

        //placeholder general
        $("form :input").each(function(index, elem) {
            var eId = $(elem).attr("id");
            var label = null;
            if (eId && (label = $(elem).parents("form").find("label[for=" + eId + "]")).length == 1) {
                $(elem).attr("placeholder", $(label).clone().children().remove().end().text().replace(/  +/g, ''));
                //$(label).remove();
            }
        });

        // placeholder for forget pswd
        $('.form-all input[type=\'text\'], .form-all input[type=\'email\'], .form-all input[type=\'password\']').each(function(index, elem) {
            var eId = $(elem).attr('id');
            var span = $(elem).closest('.form-line').children('span')
            if (span.length) {
                $(elem).attr('placeholder', span.text().replace(/  +/g, ''));
            }
        });

        // job page placeholder
        $('.form-all input[type=\'text\'], .form-all input[type=\'email\'], .form-all input[type=\'password\']').each(function(index, elem) {
            var eId = $(elem).attr('id');
            var span = $(elem).closest('#ef-youremail-field').children('span')
            if (span.length) {
                $(elem).attr('placeholder', span.text().replace(/  +/g, ''));
            }
        });

        $('.form-all input[type=\'text\'], .form-all input[type=\'email\'], .form-all input[type=\'password\']').each(function(index, elem) {
            var eId = $(elem).attr('id');
            var span = $(elem).closest('.form-line').children('span')
            if (span.length) {
                $(elem).attr('placeholder', span.text().replace(/  +/g, ''));
            }
        });

        //change news title to blog
        var className = window.location.search.split("category=")[1];
        $("body").addClass(className);
        if (className) $("h1").text(className.replace(/-/ig, " "));

        // add link to figure in news page
        $(".jxt-news-item").each(function() {
            $(this).find('.jxt-news-item-image').wrapInner('<a href="#" class="news-link"> </a>');
            $(this).find('.jxt-news-item-image a').attr('href', $(this).find('.jxt-news-item-title > a').attr('href'));
        });

        //pagination in news
        $(".jxt-news-next").insertAfter(".jxt-news-pagination ul:last");

        //blog page chamges
        $(".blog .jxt-news-filter-summary").text("Sorty by");

        $(".blog #tbKeywords").attr("placeholder", "Enter keywords");

        $(".jxt-news-item").each(function() {
            $(this).find('.jxt-news-item-title').insertAfter($(this).find(".jxt-news-item-image"));
        });

        if ($(".jxt-single-item").length) { $('body').addClass("news-inner") };
        if(window.location.pathname.indexOf('/news') > -1){if($("body").hasClass("webinars")){$("body").addClass("blog");}}
        $("#ctl00_ContentPlaceHolder1_hLinkCategory").text("Go Back");
        
        $(".jxt-news-item-category dd a").attr('href');
        var attr_go_bk = $(".jxt-news-item-category dd a").attr('href');

        if ($('#ctl00_ContentPlaceHolder1_hLinkCategory').length && attr_go_bk.indexOf('/blog/') > -1) {
            $('#ctl00_ContentPlaceHolder1_hLinkCategory').attr('href', '/news.aspx?category=blog')
        }
        if ($('#ctl00_ContentPlaceHolder1_hLinkCategory').length && attr_go_bk.indexOf('/webinars/') > -1) {
            $('#ctl00_ContentPlaceHolder1_hLinkCategory').attr('href', '/news.aspx?category=webinars')
        }
        $("<span class='share-text'>Share</span>").insertBefore(".jxt-news-item-share ul");

        //add place holder inner advance search page
        $('#txtSalaryLowerBand').attr('placeholder', 'Minimum');
        $('#txtSalaryUpperBand').attr('placeholder', 'Maximum');



        //remove time from job
        var datepost = $(".mydate").text().trim().split(" ")[0];
        $(".mydate").text(datepost);
        //add active class on menu
        $("li a[href='" + (window.location.pathname + window.location.search).toLowerCase() + "']").parent().addClass("active");
        $("li.active li.active").parent().closest("li.active").removeClass("active");
        $("li li.active").parent().parent().addClass("active")

        //News placeholder
        $("#ctl00_ContentPlaceHolder1_tbKeywords").attr("placeholder", "Keywords");

        // 8. System pages changes
        // ========================================
        if ($('#site-topnav .user-loggedIn').length) {
            $('a#HiddenMemLog').prop("href", "/member/default.aspx").text('My Dashboard');
        }

        // Custom Function
        CustomFunction();
        if ($("body").hasClass("clients") || $("body").hasClass("submit-vacancy") || $("body").hasClass("testimonials")) {
            $('<li><a href="/meet-the-team" class="meet-the-team">Meet the Team</a></li>').insertBefore($("#prefix_left-navigation ul ul li:first-child"));
            $('<li><a href="/primary-news" class="news-nav">News</a></li>').insertBefore($("#prefix_left-navigation ul ul li:last-child"));
        }
        /* Changing Banner Images on Meet the team page */
              $("#consult-tab li a").click(function(){
               var body_class = $(this).text();
               if(body_class == 'NSW'){
                 $('body').removeClass('victoria queensland perth').addClass('nsw');

               }
               if(body_class == 'Victoria'){
                 $('body').removeClass('nsw queensland perth').addClass('victoria');
               }
               if(body_class == 'Queensland'){
                 $('body').removeClass('nsw victoria perth').addClass('queensland');
               }
                if(body_class == 'Perth'){
                 $('body').removeClass('nsw queensland perth').addClass('perth');
               }
             });
         /* Changing Banner Images on Meet the team page */     
    }); // end of document ready
})(jQuery);

function CustomFunction() {
    //console.log('this is triggered before ' + pageurl);
    var pageurl = window.location.pathname.toLowerCase();
    if (pageurl == "/member/createjobalert.aspx") {
        //basicProfile section
        $('#search-salary label[for="ctl00_ContentPlaceHolder1_ucJobAlert1_ddlSalary"]').text('Salary Type');
    }
}
/* Function for arranging all the consultants alphabetically as per the request*/
function sortUsingNestedText(parent, childSelector, keySelector) {
    var items = parent.children(childSelector).sort(function(a, b) {
        var vA = $(keySelector, a).text();
        var vB = $(keySelector, b).text();
        return (vA < vB) ? -1 : (vA > vB) ? 1 : 0;
    });
    parent.append(items);
}
/* Function for arranging all the consultants alphabetically as per the request*/